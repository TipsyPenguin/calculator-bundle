# Calculator Bundle

Required:
- PHP: >=5.3.9
- symfony/framework-bundle: >=2.7

Installation:

`composer require tipsy-penguin/calculator-bundle`
<?php

namespace TipsyPenguin\CalculatorBundle\Test\Calculator;

use PHPUnit\Framework\TestCase;
use TipsyPenguin\CalculatorBundle\Calculator\EvalCalculator;
use TipsyPenguin\CalculatorBundle\Validator\Validator;

/**
 * Class EvalCalculatorTest
 * @package TipsyPenguin\CalculatorBundle\Test\Calculator
 */
class EvalCalculatorTest extends TestCase
{
    /**
     * @var EvalCalculator
     */
    private $calculator;

    /**
     * EvalCalculatorTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        $validator = new Validator();
        $this->calculator = new EvalCalculator($validator);
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @throws \TipsyPenguin\CalculatorBundle\Exception\InvalidOperationException
     * @throws \TipsyPenguin\CalculatorBundle\Exception\InvalidTypeException
     */
    public function testSuccessCalculate()
    {
        $successOperations = array(
            "1+1" => 2,
            "2+1+7" => 10,
            "7-5" => 2,
            "1+5-2" => 4,
            "15+1-2-5" => 9,
            "4*4" => 16,
            "16/4" => 4
        );

        foreach ($successOperations as $successOperation => $result) {
            $this->assertEquals($result, $this->calculator->calculate($successOperation));
        }
    }

    /**
     * @throws \TipsyPenguin\CalculatorBundle\Exception\InvalidOperationException
     * @throws \TipsyPenguin\CalculatorBundle\Exception\InvalidTypeException
     */
    public function testPriorityOperations()
    {
        $successOperations = array(
            "2+3*4" => 14,
            "4*4-1" => 15,
            "1+4*6-5" => 20,
            "15/5*2" => 6,
            "1/3*3" => 1,
            "15/24*3" => 1.875,
            "12-32/54*5+16" => 25.037037037037
        );

        foreach ($successOperations as $successOperation => $result) {
            $this->assertEquals($result, $this->calculator->calculate($successOperation));
        }
    }

    /**
     * @throws \TipsyPenguin\CalculatorBundle\Exception\InvalidOperationException
     * @throws \TipsyPenguin\CalculatorBundle\Exception\InvalidTypeException
     */
    public function testDivisionByZero()
    {
        try {
            $this->calculator->calculate('1/0');
        } catch(\Exception $exception) {
            $this->assertTrue(true);
        }

    }
}
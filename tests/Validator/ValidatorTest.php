<?php

namespace TipsyPenguin\CalculatorBundle\Test;

use PHPUnit\Framework\TestCase;
use TipsyPenguin\CalculatorBundle\Exception\InvalidOperationException;
use TipsyPenguin\CalculatorBundle\Exception\InvalidTypeException;
use TipsyPenguin\CalculatorBundle\Validator\Validator;

/**
 * Class ValidatorTest
 * @package TipsyPenguin\CalculatorBundle\Test
 */
class ValidatorTest extends TestCase
{
    /**
     * @throws \TipsyPenguin\CalculatorBundle\Exception\InvalidOperationException
     * @throws \TipsyPenguin\CalculatorBundle\Exception\InvalidTypeException
     */
    public function testValidateInteger()
    {
        $this->expectException('\\TipsyPenguin\\CalculatorBundle\\Exception\\InvalidTypeException');
        $validator = new Validator();
        $validator->validate(1234);
    }

    /**
     * @throws \TipsyPenguin\CalculatorBundle\Exception\InvalidOperationException
     * @throws \TipsyPenguin\CalculatorBundle\Exception\InvalidTypeException
     */
    public function testValidateArray()
    {
        $this->expectException('\\TipsyPenguin\\CalculatorBundle\\Exception\\InvalidTypeException');
        $validator = new Validator();
        $validator->validate(array('1', '+', '3'));
    }

    /**
     * @throws InvalidTypeException
     */
    public function testInvalidOperation()
    {
        $validator = new Validator();
        $failedOperations = array(
            0 => 'e~@#[/}',
            1 => '123+*123',
            2 => '*1213',
            3 => '123-',
            4 => '123**123'
        );

        $count = 0;
        foreach ($failedOperations as $failedOperation) {
            try {
                $validator->validate($failedOperation);
            } catch (InvalidOperationException $exception) {
                $count++;
            }
        }

        $this->assertEquals(count($failedOperations), $count);
    }

    /**
     * @throws InvalidOperationException
     * @throws InvalidTypeException
     */
    public function testValidOperations()
    {
        $validator = new Validator();

        $successOperations = array(
            0 => '123',
            1 => '123*+123',
            2 => '-123-+444',
            3 => '+123--55',
            4 => '-55'
        );

        foreach ($successOperations as $successOperation) {
            $this->assertNull($validator->validate($successOperation));
        }
    }
}
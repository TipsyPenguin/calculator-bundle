<?php


namespace TipsyPenguin\CalculatorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package TipsyPenguin\CalculatorBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('tipsy-penguin_calculator');
        $rootNode = $treeBuilder->getRootNode();
        return $treeBuilder;
    }
}
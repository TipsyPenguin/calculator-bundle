<?php

namespace TipsyPenguin\CalculatorBundle\Exception;

use Exception;
use Throwable;

/**
 * Class InvalidTypeException
 * @package TipsyPenguin\CalculatorBundle\Exception
 */
class InvalidTypeException extends Exception
{
    /**
     * InvalidTypeException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Invalid type", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
<?php

namespace TipsyPenguin\CalculatorBundle\Exception;

use Throwable;

/**
 * Class InvalidOperationException
 * @package TipsyPenguin\CalculatorBundle\Exception
 */
class InvalidOperationException extends \Exception
{
    /**
     * InvalidOperationException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Invalid operation", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
<?php

namespace TipsyPenguin\CalculatorBundle\Calculator;

use TipsyPenguin\CalculatorBundle\Exception\InvalidOperationException;
use TipsyPenguin\CalculatorBundle\Exception\InvalidTypeException;

/**
 * Interface Calculator
 * @package TipsyPenguin\CalculatorBundle\Calculator
 */
interface Calculator
{
    /**
     * @param $value
     * @return int
     *
     * @throws InvalidTypeException
     * @throws InvalidOperationException
     */
    public function calculate($value);
}
<?php

namespace TipsyPenguin\CalculatorBundle\Calculator;

use TipsyPenguin\CalculatorBundle\Validator\Validator;

/**
 * Class EvalCalculator
 * @package TipsyPenguin\CalculatorBundle\Calculator
 */
class EvalCalculator implements Calculator
{
    /**
     * @var Validator
     */
    private $validator;

    /**
     * EvalCalculator constructor.
     * @param Validator $validator
     */
    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param $value
     * @return int
     * @throws \TipsyPenguin\CalculatorBundle\Exception\InvalidOperationException
     * @throws \TipsyPenguin\CalculatorBundle\Exception\InvalidTypeException
     */
    public function calculate($value)
    {
        $this->validator->validate($value);

        $code = "return ${value};";
        return eval($code);
    }
}
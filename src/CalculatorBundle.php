<?php

namespace TipsyPenguin\CalculatorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use TipsyPenguin\CalculatorBundle\DependencyInjection\CalculatorExtension;

/**
 * Class CalculatorBundle
 * @package TipsyPenguin\CalculatorBundle
 */
class CalculatorBundle extends Bundle
{
    /**
     * @return \Symfony\Component\DependencyInjection\Extension\ExtensionInterface|CalculatorExtension|null
     */
    public function getContainerExtension()
    {
        if (!$this->extension) {
            $this->extension = new CalculatorExtension();
        }

        return $this->extension;
    }
}
<?php

namespace TipsyPenguin\CalculatorBundle\Validator;

use TipsyPenguin\CalculatorBundle\Exception\InvalidOperationException;
use TipsyPenguin\CalculatorBundle\Exception\InvalidTypeException;

/**
 * Class Validator
 * @package TipsyPenguin\CalculatorBundle\Validator
 */
class Validator
{
    /**
     * @param $value
     * @throws InvalidTypeException
     * @throws InvalidOperationException
     */
    public function validate($value)
    {
        if (!is_string($value)) {
            throw new InvalidTypeException();
        }

        if (!preg_match("/^([-+]??(\d+|\(\g<1>\))( ?[-+*\/] ?\g<1>)?)$/", $value)) {
            throw new InvalidOperationException();
        }
    }
}
